# Requirements  
Using the Azure Resource Manager modules requires having specific Azure SDK modules installed on the host running Ansible.  
```
$ pip3 install 'ansible[azure]'
```
Edit credentials.ini and place in ~/.azure/credentials  
If your secret values contain non-ASCII characters, you must URL Encode them to avoid login errors.  

To use an Azure Cloud other than the default public cloud (eg, Azure China Cloud, Azure US Government Cloud, Azure Stack), pass the “cloud_environment” argument to modules, configure it in a credential profile, or set the “AZURE_CLOUD_ENVIRONMENT” environment variable. The value is either a cloud name as defined by the Azure Python SDK (eg, “AzureChinaCloud”, “AzureUSGovernment”; defaults to “AzureCloud”) or an Azure metadata discovery URL (for Azure Stack).
