The Azure Resource Manager inventory script is called azure_rm.py. It authenticates with the Azure API exactly the same as the Azure modules, which means you will  define the environment variables in a $HOME/.azure/credentials file (describedin YAML file folder), or pass command line parameters. To see available command line options execute the following:

$ ./azure_rm.py --help


You can control host groupings and host selection by creating an azure_rm.ini file in your current working directory.

Control grouping using the following variables defined in the environment:  
  AZURE_GROUP_BY_RESOURCE_GROUP=yes  
  AZURE_GROUP_BY_LOCATION=yes  
  AZURE_GROUP_BY_SECURITY_GROUP=yes  
  AZURE_GROUP_BY_TAG=yes  
  AZURE_GROUP_BY_OS_FAMILY=yes    

Select hosts within specific resource groups by assigning a comma separated list to:  
  AZURE_RESOURCE_GROUPS=resource_group_a,resource_group_b

Select hosts for specific tag key by assigning a comma separated list of tag keys to:  
  AZURE_TAGS=key1,key2,key3

Select hosts for specific locations by assigning a comma separated list of locations to:  
  AZURE_LOCATIONS=eastus,eastus2,westus

Or, select hosts for specific tag key:value pairs by assigning a comma separated list key:value pairs to:  
  AZURE_TAGS=key1:value1,key2:value2

If you don’t need the powerstate, you can improve performance by turning off powerstate fetching:  
  AZURE_INCLUDE_POWERSTATE=no

A sample azure_rm.ini file is included along with the inventory script in here. An .ini file will contain the following:  
```
[azure]
# Control which resource groups are included. By default all resources groups are included.
# Set resource_groups to a comma separated list of resource groups names.
#resource_groups=

# Control which tags are included. Set tags to a comma separated list of keys or key:value pairs
#tags=

# Control which locations are included. Set locations to a comma separated list of locations.
#locations=

# Include powerstate. If you don't need powerstate information, turning it off improves runtime performance.
# Valid values: yes, no, true, false, True, False, 0, 1.
include_powerstate=yes

# Control grouping with the following boolean flags. Valid values: yes, no, true, false, True, False, 0, 1.
group_by_resource_group=yes
group_by_location=yes
group_by_security_group=yes
group_by_tag=yes
group_by_os_family=yes
```

## Examples  
Here are some examples using the inventory script:  
```
# Execute /bin/uname on all instances in the Testing resource group
$ ansible -i azure_rm.py Testing -m shell -a "/bin/uname -a"

# Execute win_ping on all Windows instances
$ ansible -i azure_rm.py windows -m win_ping

# Execute ping on all Linux instances
$ ansible -i azure_rm.py linux -m ping

# Use the inventory script to print instance specific information
$ ./azure_rm.py --host my_instance_host_name --resource-groups=Testing --pretty

# Use the inventory script with ansible-playbook
$ ansible-playbook -i ./azure_rm.py test_playbook.yml
```

## Here is a simple playbook to exercise the Azure inventory script:  
```
- name: Test the inventory script
  hosts: azure
  connection: local
  gather_facts: no
  tasks:
    - debug:
        msg: "{{ inventory_hostname }} has powerstate {{ powerstate }}"
```

## You can execute the playbook with something like:  

$ ansible-playbook -i ./azure_rm.py test_azure_inventory.yml  
